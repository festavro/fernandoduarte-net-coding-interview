﻿namespace SecureFlight.Api.Models;

public class FlightPassengerTransferObject
{
    public int FlightId { get; init; }

    public string PassengerId { get; init; }
}