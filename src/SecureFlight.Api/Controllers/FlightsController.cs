﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SecureFlight.Api.Models;
using SecureFlight.Api.Utils;
using SecureFlight.Core.Entities;
using SecureFlight.Core.Interfaces;

namespace SecureFlight.Api.Controllers;

[ApiController]
[Route("[controller]")]
public class FlightsController : SecureFlightBaseController
{
    private readonly IService<Flight> _flightService;
    private readonly IService<PassengerFlight> _passengerFlightService;
    private readonly IService<Passenger> _passengerService;

    public FlightsController(IService<Flight> flightService, IService<PassengerFlight> passengerFlightService, IService<Passenger> passengerService, IMapper mapper)
        : base(mapper)
    {
        _passengerFlightService = passengerFlightService;
        _flightService = flightService;
        _passengerService = passengerService;
    }

    [HttpGet]
    [ProducesResponseType(typeof(IEnumerable<FlightDataTransferObject>), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponseActionResult))]
    public async Task<IActionResult> Get()
    {
        var flights = await _flightService.GetAllAsync();
        return GetResult<IReadOnlyList<Flight>, IReadOnlyList<FlightDataTransferObject>>(flights);
    }

    [HttpDelete]
    [ProducesResponseType(typeof(IEnumerable<FlightDataTransferObject>), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponseActionResult))]
    public async Task<IActionResult> Delete(FlightPassengerTransferObject flightPassenger)
    {
        var existsEntites = await this.ValidateExistingFlightAndPassenger(flightPassenger);
        if (!existsEntites)
            return NotFound();


        var passengerFlight = new PassengerFlight()
        {
            FlightId = flightPassenger.FlightId,
            PassengerId = flightPassenger.PassengerId
        };

        await this._passengerFlightService.Remove(passengerFlight);

        var passengerFlightsResult = await _passengerFlightService.FilterAsync(p => p.PassengerId == flightPassenger.PassengerId);
        var isThereAnyMorePassengerFlights = passengerFlightsResult.Result.Any();
        var passenger = new Passenger()
        {
            Id = flightPassenger.PassengerId
        };
        if (!isThereAnyMorePassengerFlights)

            await _passengerService.Remove(passenger);
        return Ok();
    }
    
    [HttpPost]
    [ProducesResponseType(typeof(IEnumerable<FlightDataTransferObject>), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponseActionResult))]
    public async Task<IActionResult> Post(FlightPassengerTransferObject flightPassenger)
    {
        var existsEntites = await this.ValidateExistingFlightAndPassenger(flightPassenger);
        if (!existsEntites)
            return NotFound();


        var passengerFlight = new PassengerFlight()
        {
            FlightId = flightPassenger.FlightId,
            PassengerId = flightPassenger.PassengerId
        };

        var result = await this._passengerFlightService.Add(passengerFlight);
        return Ok();
    }

    private async Task<bool> ValidateExistingFlightAndPassenger(FlightPassengerTransferObject flightPassenger)
    {
        var flightResult = await _flightService.FilterAsync(f => f.Id == flightPassenger.FlightId);
        var flight = flightResult.Result.FirstOrDefault();
        if (flight == null)
            return false;

        var passengerResult = await _passengerService.FilterAsync(f => f.Id == flightPassenger.PassengerId);
        var passenger = passengerResult.Result.FirstOrDefault();
        if (passenger == null)
            return false;
        return true;
    }
    
}